import sys
from PyQt5.QtWidgets import *


class App(QDialog):
    def __init__(self):
        super().__init__()

        self.title = 'E-Liqui Kalkulátor'

        self.lblBase = QLabel('<b>Alapanyagok</b>')
        self.lblBaseNic = QLabel('Nikotinos alap:')
        self.lblPG = QLabel('<b>PG [%]</b>')
        self.lblVG = QLabel('<b>VG [%]</b>')
        self.lblWeight = QLabel('<b>Erősség [mg/ml]</b>')
        self.lblArom = QLabel('Aroma:')
        self.lblClaimedNic = QLabel('Kívánt nikotin erősség:')
        self.lblClaimedVG = QLabel('Kívánt keverég VG arány:')
        self.lblAmount = QLabel('Keverendő mennyiség:')
        self.lblNic = QLabel('')
        self.lblCentrom = QLabel('%')
        self.lblMg = QLabel('mg/ml')
        self.lblMl = QLabel('ml')
        self.lblCentClaimedVG = QLabel('%')

        self.lblRes = QLabel('<b>Alapanyag</b>')
        self.lblResMl = QLabel('<b>ml</b>')
        self.lblResBaseNic = QLabel('Nikotinos alap:')
        self.lblResPG = QLabel('PG:')
        self.lblResVG = QLabel('VG:')
        self.lblResArom = QLabel('Aroma:')

        self.editBaseNic = QLineEdit()
        self.editBaseNic.setReadOnly(True)

        self.editPG = QLineEdit()
        self.editPG.setReadOnly(True)

        self.editVG = QLineEdit()
        self.editVG.setReadOnly(True)

        self.editArom = QLineEdit()
        self.editArom.setReadOnly(True)

        self.spinBoxVG = QSpinBox()
        self.spinBoxVG.setRange(0, 100)

        self.spinBoxPG = QSpinBox()
        self.spinBoxPG.setRange(0, 100)

        self.spinBoxArom = QSpinBox()
        self.spinBoxBaseNic = QSpinBox()

        self.spinBoxClaimedVG = QSpinBox()

        self.spinBoxAmount = QSpinBox()
        self.spinBoxAmount.setRange(0, 100)

        self.spinBoxWeight = QSpinBox()
        self.spinBoxWeight.setRange(0, 100)

        self.btnCalc = QPushButton('Számolás')

        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        # self.setGeometry(100, 100, 500, 500)

        self.createGridDatasLayout()
        self.createGridResultLayout()

        windowLayout = QHBoxLayout()
        windowLayout.addWidget(self.createGridDatasLayout)
        windowLayout.addWidget(self.createGridResultLayout)
        self.setLayout(windowLayout)

        self.show()
        self.setFocus()

        self.btnCalc.clicked.connect(self.CalcRes)

    def createGridDatasLayout(self):
        self.createGridDatasLayout = QGroupBox('Alap adatok')
        layout = QGridLayout()

        layout.addWidget(self.lblBase, 0, 0)
        layout.addWidget(self.lblPG, 0, 1)
        layout.addWidget(self.lblVG, 0, 2)
        layout.addWidget(self.lblWeight, 0, 3)

        layout.addWidget(self.lblBaseNic, 1, 0)
        layout.addWidget(self.spinBoxPG, 1, 1)
        layout.addWidget(self.spinBoxVG, 1, 2)
        layout.addWidget(self.spinBoxWeight, 1, 3)

        layout.addWidget(self.lblArom, 2, 0)
        layout.addWidget(self.spinBoxArom, 2, 1)
        layout.addWidget(self.lblCentrom, 2, 2)

        layout.addWidget(self.lblClaimedNic, 3, 0)
        layout.addWidget(self.spinBoxBaseNic, 3, 1)
        layout.addWidget(self.lblMg, 3, 2)

        layout.addWidget(self.lblClaimedVG, 4, 0)
        layout.addWidget(self.spinBoxClaimedVG, 4, 1)
        layout.addWidget(self.lblCentClaimedVG, 4, 2)

        layout.addWidget(self.lblAmount, 5, 0)
        layout.addWidget(self.spinBoxAmount, 5, 1)
        layout.addWidget(self.lblMl, 5, 2)

        layout.addWidget(self.btnCalc, 6, 0)

        self.createGridDatasLayout.setLayout(layout)

    def createGridResultLayout(self):
        self.createGridResultLayout = QGroupBox('Eredmények')
        layout = QGridLayout()

        layout.addWidget(self.lblRes, 0, 0)
        layout.addWidget(self.lblResMl, 0, 1)

        layout.addWidget(self.lblResBaseNic, 1, 0)
        layout.addWidget(self.editBaseNic, 1, 1)

        layout.addWidget(self.lblResPG, 2, 0)
        layout.addWidget(self.editPG, 2, 1)

        layout.addWidget(self.lblResVG, 3, 0)
        layout.addWidget(self.editVG, 3, 1)

        layout.addWidget(self.lblResArom, 4, 0)
        layout.addWidget(self.editArom, 4, 1)

        self.createGridResultLayout.setLayout(layout)

    def CalcRes(self):
        try:
            self.editBaseNic.setText(str(self.CalculateNic()))
            self.editPG.setText(str(self.CalculatePG()))
            self.editVG.setText(str(self.CalculateVG()))
            self.editArom.setText(str(self.CalculateArom()))
        except:
            self.errorMessage = QMessageBox.question(self, 'Error',
                                                     'Töltsd ki az összes mezőt, valahol 0-val osztunk, ami nem szerencsés...',
                                                     QMessageBox.Ok)

    def CalculateArom(self):
        return float(self.spinBoxAmount.text()) / 100 * float(self.spinBoxArom.text())

    def CalculateNic(self):
        return (float(self.spinBoxAmount.text()) / (
                float(self.spinBoxWeight.text()) / float(self.spinBoxBaseNic.text()))) - (self.CalculateArom() / 3)

    def CalculatePG(self):
        return abs((float(self.spinBoxPG.text()) / 100) * self.CalculateNic() - float(self.spinBoxAmount.text()) * (
                (100 - float(self.spinBoxClaimedVG.text())) / 100)) - (self.CalculateArom() / 3)

    def CalculateVG(self):
        return abs((float(self.spinBoxVG.text()) / 100) * self.CalculateNic() - float(self.spinBoxAmount.text()) * (
                (float(self.spinBoxClaimedVG.text())) / 100)) - (self.CalculateArom() / 3)
